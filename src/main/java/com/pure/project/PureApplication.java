package com.pure.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages ="com.pure.project.mapper")
public class PureApplication {
    public static void main(String[] args) {
        SpringApplication.run(PureApplication.class, args);
    }

}
